//uScript Generated Code - Build 1.0.3101
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Capsule_Movement_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 direction = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_6_System_Single = (float) 0;
   System.Single local_7_System_Single = (float) 1;
   public System.Single speed = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_11 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_2 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_XMin_2 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_XMax_2 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_YMin_2 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_YMax_2 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_ZMin_2 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_ZMax_2 = (float) 1;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_2;
   bool logic_uScriptAct_ClampVector3_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4;
   bool logic_uScriptAct_GetDeltaTime_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_5;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_8;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_8 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_3 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_3 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_3 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_3 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_3.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_3.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_3;
                  component.OnLateUpdate += Instance_OnLateUpdate_3;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_3;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_3 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_3.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_3;
               component.OnLateUpdate -= Instance_OnLateUpdate_3;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_3;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.SetParent(g);
      owner_Connection_11 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_3(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_3( );
   }
   
   void Instance_OnLateUpdate_3(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_3( );
   }
   
   void Instance_OnFixedUpdate_3(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_3( );
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0266bb0f-b179-4d06-8425-fdf87b46a573", "Clamp_Vector3", Relay_In_2)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_2 = direction;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.In(logic_uScriptAct_ClampVector3_Target_2, logic_uScriptAct_ClampVector3_ClampX_2, logic_uScriptAct_ClampVector3_XMin_2, logic_uScriptAct_ClampVector3_XMax_2, logic_uScriptAct_ClampVector3_ClampY_2, logic_uScriptAct_ClampVector3_YMin_2, logic_uScriptAct_ClampVector3_YMax_2, logic_uScriptAct_ClampVector3_ClampZ_2, logic_uScriptAct_ClampVector3_ZMin_2, logic_uScriptAct_ClampVector3_ZMax_2, out logic_uScriptAct_ClampVector3_Result_2);
         direction = logic_uScriptAct_ClampVector3_Result_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.Out;
         
         if ( test_0 == true )
         {
            Relay_In_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Capsule_Movement_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_3()
   {
      if (true == CheckDebugBreak("5a28c829-f5aa-417a-8f7b-1e358124e55b", "Global_Update", Relay_OnUpdate_3)) return; 
      Relay_In_2();
   }
   
   void Relay_OnLateUpdate_3()
   {
      if (true == CheckDebugBreak("5a28c829-f5aa-417a-8f7b-1e358124e55b", "Global_Update", Relay_OnLateUpdate_3)) return; 
   }
   
   void Relay_OnFixedUpdate_3()
   {
      if (true == CheckDebugBreak("5a28c829-f5aa-417a-8f7b-1e358124e55b", "Global_Update", Relay_OnFixedUpdate_3)) return; 
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("904d57ed-b9ad-4397-a283-4549f450e015", "Get_Delta_Time", Relay_In_4)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_4, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4);
         local_6_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_4;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Capsule_Movement_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c881a565-7f47-4a9a-8d49-d739b7cf01bd", "Multiply_Float", Relay_In_5)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_5 = local_7_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_5 = local_6_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.In(logic_uScriptAct_MultiplyFloat_v2_A_5, logic_uScriptAct_MultiplyFloat_v2_B_5, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_5, out logic_uScriptAct_MultiplyFloat_v2_IntResult_5);
         speed = logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.Out;
         
         if ( test_0 == true )
         {
            Relay_In_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Capsule_Movement_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("59bcbcce-1d2d-4915-b0fa-cd2597f32237", "Multiply_Vector3_With_Float", Relay_In_8)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8 = direction;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8 = speed;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_8, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_8, out logic_uScriptAct_MultiplyVector3WithFloat_Result_8);
         local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_8;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_8.Out;
         
         if ( test_0 == true )
         {
            Relay_Translate_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Capsule_Movement_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bed90dec-cb03-482e-b4fd-03ec54a3d1ba", "UnityEngine_Transform", Relay_Translate_9)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9 = local_10_UnityEngine_Vector3;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_11.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Capsule_Movement_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Capsule_Movement_U.uscript:speed", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8071322c-198b-46fc-99ad-7278d5cff6c9", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Capsule_Movement_U.uscript:direction", direction);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "44410589-7a67-44c0-88fa-0911f7a95408", direction);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Capsule_Movement_U.uscript:6", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "65e57b20-fc55-4bc0-a722-ea1554b0c22d", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Capsule_Movement_U.uscript:7", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "84ee6261-3bea-4079-8100-6904c4ebf2df", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Capsule_Movement_U.uscript:10", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "1d9dd466-53f1-4d0e-b79e-3c9cca129dcb", local_10_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
