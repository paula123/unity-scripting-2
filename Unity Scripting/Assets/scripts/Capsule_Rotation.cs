﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsule_Rotation : MonoBehaviour {

    public float rotationSpeedX, rotationSpeedY, rotationSpeedZ;
   
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(rotationSpeedX, rotationSpeedY, rotationSpeedZ) * Time.deltaTime);
    }
}