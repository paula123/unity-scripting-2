﻿using UnityEngine;
using System.Collections;

public class PlayerFPSController : MonoBehaviour
{

    public GameObject CamerasParent;
    private Animator animator;
    public float walkSpeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;


    void Start()
    {

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        animator = GetComponent<Animator>();

        GameObject.Find("Capsule").GetComponent<MeshRenderer>().enabled = false;
    }

    void Update()
    {

        movement();
        aiming();

    }

    private void movement()
    {
        //Movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

        //Rotation
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;
        //vCamRotation = Mathf.Clamp(vCamRotation,-70f,70f);


        Debug.Log(hPlayerRotation);

        transform.Rotate(0f, hPlayerRotation, 0f);
        //CamerasParent.transform.rotation = Quaternion.Euler(vCamRotation, 0f, 0f);
        CamerasParent.transform.Rotate(-vCamRotation, 0f, 0f);
        //Debug.Log(CamerasParent.transform.eulerAngles.x);
        if (CamerasParent.transform.eulerAngles.x < 180)
        {
            CamerasParent.transform.localRotation = Quaternion.Euler(Mathf.Clamp(CamerasParent.transform.eulerAngles.x, 0, 70f), 0, 0);
        }
        else
        {
            CamerasParent.transform.localRotation = Quaternion.Euler(Mathf.Clamp(CamerasParent.transform.eulerAngles.x, 290f, 360f), 0, 0);
        }

        //
    }

    private void aiming()
    {
        if (Input.GetButton("Fire2"))
        {
            animator.SetBool("aiming", true);
        }
        else
        {
            animator.SetBool("aiming", false);
        }
    }
}
