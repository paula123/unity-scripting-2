﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Capsule_Scaling : MonoBehaviour
    {

        [SerializeField]

        private Vector3 axes;
        public float scaleUnits;

        // Update is called once per frame
        void Update()
        {

            axes = Capsule_Movement.ClampVector3(axes);
            transform.localScale += axes * (scaleUnits * Time.deltaTime);
        }
    }